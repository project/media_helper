<?php

declare(strict_types=1);

namespace Drupal\media_helper_twig_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller for media_helper twig testing.
 */
class TwigTestController extends ControllerBase {

  /**
   * Returns the renderable array for the twig testing route.
   */
  public function content(Request $request): array {
    return [
      '#theme' => 'media_helper_twig_test',
      '#image_media_id' => $request->query->getInt('image_media_id'),
      '#video_media_id' => $request->query->getInt('video_media_id'),
    ];
  }

}
