<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Kernel\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidator;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\media_helper\EventSubscriber\ConfigSubscriber
 *
 * @group media_helper
 */
final class ConfigSubscriberTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media_helper',
  ];

  /**
   * Tests the configAltered() method.
   */
  public function testConfigAltered(): void {
    $media_helper_config = $this->config('media_helper.settings');
    $config_cache_tags = $media_helper_config->getCacheTags();

    $mock_invalidator = $this->createMock(CacheTagsInvalidator::class);
    $mock_invalidator->expects(self::atLeast(2))
      ->method('invalidateTags')
      ->willReturnCallback(function (array $tags) use ($config_cache_tags): void {
        // We expect the cache tag(s) of the config itself to be invalidated in
        // a separate call from Core code, so we don't want to run assertions on
        // tag groups containing them.
        foreach ($config_cache_tags as $config_cache_tag) {
          if (in_array($config_cache_tag, $tags, TRUE)) {
            return;
          }
        }

        self::assertContains('rendered', $tags);
      });
    \Drupal::service('cache_tags.invalidator')->addInvalidator($mock_invalidator);

    $media_helper_config->save();

    $media_helper_config->delete();
  }

}
