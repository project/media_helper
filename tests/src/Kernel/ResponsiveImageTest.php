<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;

/**
 * Tests responsive image theme functions and output.
 *
 * @group media_helper
 */
final class ResponsiveImageTest extends KernelTestBase {

  public const TEST_ATTRIBUTES = [
    'itemprop' => 'foobar',
    'data-my-attribute' => 'my value',
  ];

  /**
   * URIs of test images.
   *
   * Copied from \Drupal\KernelTests\Core\Theme\ImageTest.
   */
  public const TEST_IMAGES = [
    'core/misc/druplicon.png',
    'core/misc/loading.gif',
  ];

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'breakpoint',
    'field',
    'file',
    'image',
    'media_helper',
    'media_helper_test_with_picture_attributes',
    'responsive_image',
    'responsive_image_test_module',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    ResponsiveImageStyle::create([
      'id' => 'foo',
      'label' => 'Foo',
      'breakpoint_group' => 'responsive_image_test_module',
    ])->save();

    $this->installConfig('media_helper');
  }

  /**
   * Tests that responsive_image output is not messed up by this module.
   */
  public function testResponsiveImageMarkup(): void {
    $image = [
      '#theme' => 'responsive_image',
      '#uri' => self::TEST_IMAGES[0],
      '#responsive_image_style_id' => 'foo',
      '#media_helper_attributes' => self::TEST_ATTRIBUTES,
    ];
    $this->render($image);

    $picture = $this->xpath('//picture');
    $this->assertCount(1, $picture, '<picture> tag is present when using media_helper with responsive_image.');
    $picture = reset($picture);
    $img = $picture->xpath('//img');
    $this->assertCount(1, $img, '<img> tag is present when using media_helper with responsive_image.');
    $img = reset($img);

    $this->assertSame(self::TEST_ATTRIBUTES['itemprop'], $img->attributes()->itemprop?->__toString(), 'Attributes set to be passed to the <img> tag are present there.');
    $this->assertNull($picture->attributes()->itemprop, 'Attributes set to be passed to the <img> tag are not present on <picture>.');

    $this->assertSame(self::TEST_ATTRIBUTES['data-my-attribute'], $picture->attributes()->{'data-my-attribute'}?->__toString(), 'Attributes set to be passed to the <picture> tag are present there.');
    $this->assertNull($img->attributes()->{'data-my-attribute'}, 'Attributes set to be passed to the <picture> tag are not present on <img>.');
  }

}
