<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Kernel\Service;

use Drupal\image\Entity\ImageStyle;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media_helper\MediaHelperInterface;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\media_helper\Traits\GenerateMediaTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests integration with the svg_image module.
 *
 * @group media_helper
 */
final class MediaHelperSvgImageTest extends KernelTestBase {

  use GenerateMediaTrait;
  use MediaTypeCreationTrait;
  use UserCreationTrait;

  /**
   * File name of generated test image.
   */
  public const TEST_FILE_NAME = 'test.svg';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'file',
    'image',
    'media',
    'media_helper',
    'svg_image',
    'system',
    'user',
  ];

  /**
   * The media_helper service.
   */
  protected MediaHelperInterface $mediaHelper;

  /**
   * SVG media entity to test with.
   */
  protected ?MediaInterface $imageMediaEntity = NULL;

  /**
   * The media type used for this test.
   */
  protected ?MediaTypeInterface $mediaType = NULL;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('media_type');
    $this->installEntitySchema('media');
    $this->installConfig(['system', 'media', 'media_helper']);

    $this->setUpCurrentUser([], ['view media']);

    $this->mediaHelper = $this->container->get('media_helper');

    $this->mediaType = $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);

    $this->imageMediaEntity = $this->generateMedia(self::TEST_FILE_NAME, $this->mediaType, '<svg version = "1.1"
     width="123" height="70"
     xmlns="http://www.w3.org/2000/svg">
  <rect x="20" y="20" width="80" height="40" fill="black" />
</svg>');
  }

  /**
   * Tests that media_helper integration with svg_image works.
   */
  public function testSvgImageField(): void {
    $image = $this->mediaHelper->mediaImage($this->imageMediaEntity);
    $this->render($image);

    $img = $this->xpath('//img');
    $this->assertCount(1, $img, '<img> tag is present when using media_helper with svg_image.');
    $img = reset($img);
    $this->assertTrue(str_contains($img->attributes()->src?->__toString(), self::TEST_FILE_NAME), '<img> tag src attribute should contain image file name when using media_helper with svg_image.');
  }

  /**
   * Tests that dimension integration with svg_image works.
   */
  public function testSvgImageFieldImageStyleIntegration(): void {
    $this->markTestSkipped('svg_image module does not set width and height attributes, so this functionality will never work.');

    // Test that dimensions are set correctly with no image style.
    $image = $this->mediaHelper->mediaImage($this->imageMediaEntity);
    $this->render($image);

    $img = $this->xpath('//img');
    $this->assertCount(1, $img, '<img> tag is present when using media_helper with svg_image.');
    $img = reset($img);
    $this->assertSame('123', $img->attributes()->width?->__toString(), '<img> tag width should respect file item dimensions when using media_helper with svg_image.');
    $this->assertSame('70', $img->attributes()->height?->__toString(), '<img> tag height should respect file item dimensions when using media_helper with svg_image.');

    $scale_image_style_name = 'test_scale';
    $scale_image_style = ImageStyle::create([
      'name' => $scale_image_style_name,
      'label' => 'Scaled image style',
    ]);
    $scale_image_style->addImageEffect([
      'id' => 'image_scale',
      'data' => [
        'width' => 75,
        'height' => 75,
        'upscale' => FALSE,
      ],
    ]);
    $scale_image_style->save();

    $image = $this->mediaHelper->mediaImage($this->imageMediaEntity, $scale_image_style_name);
    $this->render($image);

    $img = $this->xpath('//img');
    $this->assertCount(1, $img, '<img> tag is present when using media_helper with svg_image.');
    $img = reset($img);
    $this->assertSame('75', $img->attributes()->width?->__toString(), '<img> tag width should respect the given image style when using media_helper with svg_image.');
    $this->assertSame('43', $img->attributes()->height?->__toString(), '<img> tag height should respect the given image style when using media_helper with svg_image.');
  }

}
