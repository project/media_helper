<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Kernel\Service;

use Drupal\KernelTests\KernelTestBase;
use Drupal\media\MediaInterface;
use Drupal\media_helper\MediaHelperInterface;
use Drupal\responsive_image\Entity\ResponsiveImageStyle;
use Drupal\responsive_image\ResponsiveImageStyleInterface;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\media_helper\Traits\GenerateMediaTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;

/**
 * Tests integration between the media_helper service and the responsive_image module.
 *
 * @group media_helper
 */
final class MediaHelperResponsiveImageTest extends KernelTestBase {

  use GenerateMediaTrait;
  use MediaTypeCreationTrait;
  use UserCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'breakpoint',
    'field',
    'file',
    'image',
    'media',
    'media_helper',
    'responsive_image',
    'responsive_image_test_module',
    'system',
    'user',
  ];

  /**
   * The media_helper service.
   */
  protected MediaHelperInterface $mediaHelper;

  /**
   * Image media entity to test with.
   */
  protected ?MediaInterface $imageMediaEntity = NULL;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('media_type');
    $this->installEntitySchema('media');
    $this->installConfig(['system', 'media', 'media_helper']);

    $this->setUpCurrentUser([], ['view media']);

    $media_type = $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);
    $this->imageMediaEntity = $this->generateMedia('test.png', $media_type);

    $this->mediaHelper = $this->container->get('media_helper');
  }

  /**
   * Tests that media_helper integration with svg_image_field works.
   */
  public function testResponsiveImageStyle(): void {
    $responsive_image_style = ResponsiveImageStyle::create([
      'id' => 'foo',
      'label' => 'Foo',
      'breakpoint_group' => 'responsive_image_test_module',
      'fallback_image_style' => ResponsiveImageStyleInterface::ORIGINAL_IMAGE,
    ]);
    foreach ([
      'responsive_image_test_module.mobile',
      'responsive_image_test_module.narrow',
      'responsive_image_test_module.wide',
    ] as $breakpoint_id) {
      $responsive_image_style->addImageStyleMapping($breakpoint_id, '1x', [
        'image_mapping_type' => 'image_style',
        'image_mapping' => ResponsiveImageStyleInterface::ORIGINAL_IMAGE,
      ]);
    }
    $responsive_image_style->save();

    $image = $this->mediaHelper->mediaImage($this->imageMediaEntity, 'foo');
    $this->render($image);

    $picture = $this->xpath('//picture');
    $this->assertCount(1, $picture, '<picture> tag is present as necessary when using media_helper with responsive_image style.');
    $picture = reset($picture);
    $this->assertCount(3, $picture->xpath('//source'), 'The correct responsive image style is used when using media_helper with responsive_image style.');
  }

}
