<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Kernel\Service;

use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\media_helper\MediaHelperInterface;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\media_helper\Traits\GenerateMediaTrait;
use Drupal\user\Entity\User;
use Drupal\user\UserInterface;

/**
 * Tests responsive image theme functions and output.
 *
 * @coversDefaultClass \Drupal\media_helper\Service\MediaHelper
 *
 * @group media_helper
 */
final class MediaHelperTest extends KernelTestBase {

  use GenerateMediaTrait;
  use MediaTypeCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'breakpoint',
    'field',
    'file',
    'image',
    'media',
    'media_helper',
    'svg_image_field',
    'system',
    'user',
  ];

  /**
   * The current_user service.
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The media_helper service.
   */
  protected MediaHelperInterface $mediaHelper;

  /**
   * The test user.
   */
  protected UserInterface $user;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
    $this->installEntitySchema('file');
    $this->installSchema('file', 'file_usage');
    $this->installEntitySchema('media');
    $this->installConfig(['system', 'field', 'image', 'file', 'media', 'media_helper']);

    $this->currentUser = $this->container->get('current_user');

    $this->user = User::create([
      'name' => 'username',
      'status' => 1,
    ]);
    $this->user->save();

    $this->mediaHelper = $this->container->get('media_helper');
  }

  /**
   * Tests the function that retrieves supported image source IDs.
   */
  public function testGetSupportedImageSourcePluginIds(): void {
    $supported_plugin_ids = $this->mediaHelper->getSupportedImageSourcePluginIds();
    $this->assertContains('image', $supported_plugin_ids, 'The image source should be supported for images.');
  }

  /**
   * Tests the function that checks a source ID for image output support.
   */
  public function testIsImageSourceSupported(): void {
    $this->assertTrue($this->mediaHelper->isImageSourceSupported('image'), 'The image source should be supported for images.');

    $source_manager = $this->container->get('plugin.manager.media.source');
    assert($source_manager instanceof PluginManagerInterface);
    $image_source = $source_manager->createInstance('image');
    $this->assertTrue($this->mediaHelper->isImageSourceSupported($image_source), 'isImageSourceSupported() should accept source plugin instances as an argument.');
  }

  /**
   * Tests the function that checks if a module integration is enabled.
   */
  public function testIsModuleIntegrationEnabled(): void {
    $this->assertTrue($this->mediaHelper->isModuleIntegrationEnabled('svg_image_field'));
  }

  /**
   * Tests the media image rendering helper method.
   */
  public function testMediaImage(): void {
    $media_type = $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);

    $image_media_entity = $this->generateMedia('my-test-file.jpg', $media_type);

    $this->assertFalse($image_media_entity->access('view'), 'Test is in a state where user cannot view the image media.');
    $output = $this->mediaHelper->mediaImage($image_media_entity);
    $this->render($output);
    $this->assertCount(0, $this->xpath('//img'), '<img> tags should not be rendered when user has no view access on the image.');

    $this->currentUser->setAccount($this->user);
    $this->assertTrue($image_media_entity->access('view'), 'Test is in a state where user can view the image media.');

    $output = $this->mediaHelper->mediaImage($image_media_entity);
    $this->render($output);
    $this->assertCount(1, $this->xpath('//img'), 'An <img> tag should be rendered when user has view access on the image.');

    $output = $this->mediaHelper->mediaImage(array_fill(0, 3, $image_media_entity));
    $this->render($output);
    $this->assertCount(3, $this->xpath('//img'), 'When user has view access on the images involved, as many <img> tags as media entities were input should be rendered.');

    // Null should be accepted as input and result in null in return.
    $this->assertNull($this->mediaHelper->mediaImage(NULL));
  }

  /**
   * Tests the media image URL rendering helper method.
   */
  public function testMediaImageUrl(): void {
    $media_type = $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);

    $test_file_name = 'my-test-file.jpg';
    $image_media_entity = $this->generateMedia($test_file_name, $media_type);

    $this->assertFalse($image_media_entity->access('view'), 'Test is in a state where user cannot view the image media.');
    $output = $this->mediaHelper->mediaImageUrl($image_media_entity);
    $this->render($output);
    $this->assertNoText($test_file_name, 'Image URLs should not be rendered when user has no view access on the image.');

    $this->currentUser->setAccount($this->user);
    $this->assertTrue($image_media_entity->access('view'), 'Test is in a state where user can view the image media.');

    $output = $this->mediaHelper->mediaImageUrl($image_media_entity);
    $this->render($output);
    $this->assertUniqueText($test_file_name, 'Image URL should be rendered when user has view access on the image.');

    $output = $this->mediaHelper->mediaImageUrl(array_fill(0, 3, $image_media_entity));
    $this->render($output);
    $this->assertSame(3, substr_count($this->getRawContent(), $test_file_name), 'When user has view access on the images involved, as many image URLs as media entities were input should be rendered.');

    // Null should be accepted as input and result in null in return.
    $this->assertNull($this->mediaHelper->mediaImageUrl(NULL));
  }

  /**
   * Tests the media video rendering helper method.
   */
  public function testMediaVideo(): void {
    $media_type = $this->createMediaType('video_file', ['id' => 'video', 'label' => 'Video']);

    $video_media_entity = $this->generateMedia('my-test-file.mp4', $media_type);

    $this->assertFalse($video_media_entity->access('view'), 'Test is in a state where user cannot view the video media.');
    $output = $this->mediaHelper->mediaVideo($video_media_entity);
    $this->render($output);
    $this->assertCount(0, $this->xpath('//video'), '<video> tags should not be rendered when user has no view access on the video.');

    $this->currentUser->setAccount($this->user);
    $this->assertTrue($video_media_entity->access('view'), 'Test is in a state where user can view the video media.');

    $output = $this->mediaHelper->mediaVideo($video_media_entity);
    $this->render($output);
    $this->assertCount(1, $this->xpath('//video'), 'A <video> tag should be rendered when user has view access on the video.');

    $output = $this->mediaHelper->mediaVideo(array_fill(0, 3, $video_media_entity));
    $this->render($output);
    $this->assertCount(3, $this->xpath('//video'), 'When user has view access on the videos involved, as many <video> tags as media entities were input should be rendered.');
  }

}
