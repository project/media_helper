<?php

namespace Drupal\Tests\media_helper\Functional\Service;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests the media_helper_rendered_image field formatter.
 *
 * @group media_helper
 */
final class RenderedImageTest extends BrowserTestBase {

  use MediaTypeCreationTrait;
  use TestFileCreationTrait;

  public const MEDIA_FIELD_NAME = 'field_media_test';

  public const NODE_TYPE = 'article';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'image',
    'media',
    'media_helper',
    'node',
  ];

  /**
   * The display repository service.
   */
  protected EntityDisplayRepositoryInterface $displayRepository;

  /**
   * The test image style.
   */
  protected ImageStyleInterface $imageStyle;

  /**
   * The test image file.
   */
  protected FileInterface $testFile;

  /**
   * The test media.
   */
  protected MediaInterface $testMedia;

  /**
   * The test node.
   */
  protected NodeInterface $testNode;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->displayRepository = \Drupal::service('entity_display.repository');

    // Create image media type and an upload & media entity.
    $this->createMediaType('image', ['id' => 'image', 'label' => 'Image']);

    $this->imageStyle = ImageStyle::create([
      'name' => 'test_style',
      'label' => 'Test image style',
    ]);
    $this->imageStyle->save();

    $test_image_files = $this->getTestFiles('image');
    $this->testFile = File::create([
      'uri' => $test_image_files[0]->uri,
      'uuid' => '5dd794d0-cb75-4130-9296-838aebc1fe74',
      'status' => FileInterface::STATUS_PERMANENT,
    ]);
    $this->testFile->save();

    $this->testMedia = Media::create([
      'bundle' => 'image',
      'name' => 'Test image',
      'field_media_image' => ['target_id' => $this->testFile->id()],
    ]);
    $this->testMedia->save();

    // Create node type and media field.
    $this->drupalCreateContentType(['type' => self::NODE_TYPE, 'name' => 'Article']);

    FieldStorageConfig::create([
      'field_name' => self::MEDIA_FIELD_NAME,
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'settings' => [
        'target_type' => 'media',
      ],
      'cardinality' => 1,
    ])->save();

    FieldConfig::create([
      'field_name' => self::MEDIA_FIELD_NAME,
      'label' => self::MEDIA_FIELD_NAME,
      'entity_type' => 'node',
      'bundle' => self::NODE_TYPE,
      'required' => FALSE,
      'settings' => [
        'handler' => 'default:media',
      ],
    ])->save();

    $this->testNode = Node::create([
      'type' => self::NODE_TYPE,
      'title' => 'Media helper formatter testing',
      self::MEDIA_FIELD_NAME => $this->testMedia->id(),
    ]);
    $this->testNode->save();
  }

  /**
   * Tests the formatter output.
   */
  public function testFormatter(): void {
    $this->displayRepository->getViewDisplay('node', self::NODE_TYPE)
      ->setComponent(self::MEDIA_FIELD_NAME, [
        'type' => 'media_helper_rendered_image',
        'settings' => [
          'image_style' => $this->imageStyle->id(),
          'css_classes' => 'my-class another-class',
          'image_attributes' => '{ "id": "formatter-image" }',
        ],
      ])
      ->save();

    $this->drupalGet("/node/{$this->testNode->id()}");

    // Check element attributes are as expected based on formatter settings.
    $this->assertSession()->elementAttributeContains('css', '#formatter-image', 'src', $this->imageStyle->id());
    $this->assertSession()->elementExists('css', '#formatter-image.my-class.another-class');

    // Check cache tags.
    foreach ([
      $this->imageStyle,
      $this->testFile,
      $this->testMedia,
    ] as $entity_with_cache_tags) {
      foreach ($entity_with_cache_tags->getCacheTags() as $tag) {
        $this->assertSession()->responseHeaderContains('X-Drupal-Cache-Tags', $tag);
      }
    }
  }

}
