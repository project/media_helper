<?php

namespace Drupal\Tests\media_helper\Functional\Service;

use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\file\FileInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\image\ImageStyleInterface;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\media\Traits\MediaTypeCreationTrait;
use Drupal\Tests\media_helper\Traits\GenerateMediaTrait;
use Drupal\Tests\TestFileCreationTrait;

/**
 * Tests the media_helper twig extension.
 *
 * @group media_helper
 */
final class TwigExtensionTest extends BrowserTestBase {

  use GenerateMediaTrait;
  use MediaTypeCreationTrait;
  use TestFileCreationTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'image',
    'media',
    'media_helper',
    'media_helper_twig_test',
  ];

  /**
   * The file system service.
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The test image style.
   */
  protected ImageStyleInterface $imageStyle;

  /**
   * The test image media.
   */
  protected MediaInterface $imageMedia;

  /**
   * The test video media.
   */
  protected MediaInterface $videoMedia;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->fileSystem = $this->container->get('file_system');

    $test_image_files = $this->getTestFiles('image');

    $this->createMediaType('image', ['id' => 'image_test', 'label' => 'Image test']);
    $video_media_type = $this->createMediaType('video_file', ['id' => 'video_test', 'label' => 'Video test']);

    $this->imageStyle = ImageStyle::create([
      'name' => 'test_style',
      'label' => 'Test image style',
    ]);
    $this->imageStyle->save();

    $media_file = File::create([
      'uri' => $test_image_files[0]->uri,
      'uuid' => '5dd794d0-cb75-4130-9296-838aebc1fe74',
      'status' => FileInterface::STATUS_PERMANENT,
    ]);
    $media_file->save();

    $this->imageMedia = Media::create([
      'bundle' => 'image_test',
      'name' => 'Image 1',
      'field_media_image' => ['target_id' => $media_file->id()],
    ]);
    $this->imageMedia->save();

    $this->videoMedia = $this->generateMedia('video.mp4', $video_media_type);
    $this->videoMedia->save();
  }

  /**
   * Tests output produced by the Twig extension.
   */
  public function testOutput(): void {
    $test_image_files = $this->getTestFiles('image');

    $this->drupalGet('media-helper-twig-test', [
      'query' => [
        'image_media_id' => $this->imageMedia->id(),
        'video_media_id' => $this->videoMedia->id(),
      ],
    ]);

    $this->assertSession()->elementTextEquals('css', '#media-bundle', 'image_test');

    $this->assertSession()->elementTextEquals('css', '#media-source', 'image');

    $this->assertSession()->elementAttributeContains('css', '#media-image', 'src', $test_image_files[0]->filename);
    $this->assertSession()->elementAttributeContains('css', '#media-image', 'class', 'my-class');
    $this->assertSession()->elementAttributeContains('css', '#media-image', 'data-arbitrary', 'true');

    $this->assertSession()->elementTextContains('css', '#media-image-url', $test_image_files[0]->filename);

    $this->assertSession()->elementTextContains('css', '#media-image-url-derivative', $test_image_files[0]->filename);
    $this->assertSession()->elementTextContains('css', '#media-image-url-derivative', $this->imageStyle->id());

    $this->assertSession()->elementTextContains('css', '#media-first-nonempty', $test_image_files[0]->filename);

    $this->assertSession()->elementAttributeContains('css', '#media-video', 'class', 'my-class');
    $this->assertSession()->elementAttributeContains('css', '#media-video', 'width', '200');
    $this->assertSession()->elementAttributeContains('css', '#media-video', 'height', '300');
    $this->assertSession()->elementAttributeContains('css', '#media-video source', 'src', 'video.mp4');
  }

}
