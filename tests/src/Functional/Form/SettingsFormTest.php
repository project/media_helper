<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Functional\Form;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests Media Helper's settings form.
 *
 * @group media_helper
 */
final class SettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'media_helper',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests the module's settings form.
   */
  public function testForm(): void {
    $admin_user = $this->drupalCreateUser([
      'administer site configuration',
    ]);
    $this->drupalLogin($admin_user);

    $integrations_checkboxes = [
      'integrations[svg_image][enable]',
      'integrations[svg_image_field][enable]',
      'integrations[svg_image_field][override_dimensions]',
    ];

    // Save an initial set of values.
    $img_tag_attributes = "data-my-attribute\ndata-nifty";
    $this->drupalGet('admin/config/media/media-helper');
    $form_data = [
      'integrations[responsive_image][img_tag_attributes]' => $img_tag_attributes,
    ];
    foreach ($integrations_checkboxes as $integration_checkbox) {
      $form_data[$integration_checkbox] = TRUE;
    }
    $this->submitForm($form_data, 'Save configuration');

    // Check that config was saved as expected.
    $config = $this->config('media_helper.settings');
    $this->assertEquals(['data-my-attribute', 'data-nifty'], $config->get('integrations.responsive_image.img_tag_attributes'), 'Custom <img> tag attributes for Responsive Image integration should be saved.');

    // Check that saved values are displayed on form as expected.
    $this->drupalGet('admin/config/media/media-helper');
    $this->assertSession()->fieldValueEquals('integrations[responsive_image][img_tag_attributes]', $img_tag_attributes);
    foreach ($integrations_checkboxes as $integration_checkbox) {
      $this->assertSession()->checkboxChecked($integration_checkbox);
    }

    // Save another set of values.
    $form_data = [
      'integrations[responsive_image][img_tag_attributes]' => '',
    ];
    foreach ($integrations_checkboxes as $integration_checkbox) {
      $form_data[$integration_checkbox] = FALSE;
    }
    $this->submitForm($form_data, 'Save configuration');

    // Check the new set of values.
    $this->drupalGet('admin/config/media/media-helper');
    $this->assertSession()->fieldValueEquals('integrations[responsive_image][img_tag_attributes]', '');
    foreach ($integrations_checkboxes as $integration_checkbox) {
      $this->assertSession()->checkboxNotChecked($integration_checkbox);
    }
  }

}
