<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Traits;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;

/**
 * Provides methods to create a media type from given values.
 *
 * This trait is meant to be used only by test classes.
 */
trait GenerateMediaTrait {

  /**
   * Helper to generate a media item.
   */
  protected function generateMedia($filename, MediaTypeInterface $media_type, ?string $file_contents = NULL): MediaInterface {
    if ($file_contents === NULL) {
      $file_contents = str_repeat('a', 5);
    }

    file_put_contents('public://' . $filename, $file_contents);
    $file = File::create([
      'uri' => 'public://' . $filename,
    ]);
    $file->setPermanent();
    $file->save();

    $source_field = $media_type->getSource()->getSourceFieldDefinition($media_type);
    $file_field_name = $source_field->getName();

    return Media::create([
      'bundle' => $media_type->id(),
      'name' => 'Media Madness',
      $file_field_name => [
        'target_id' => $file->id(),
      ],
    ]);
  }

}
