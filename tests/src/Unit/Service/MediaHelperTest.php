<?php

declare(strict_types=1);

namespace Drupal\Tests\media_helper\Unit\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceInterface;
use Drupal\media_helper\Service\MediaHelper;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\media_helper\Service\MediaHelper
 *
 * @group media_helper
 */
final class MediaHelperTest extends UnitTestCase {

  /**
   * An instance of the MediaHelper service.
   */
  protected MediaHelper $mediaHelper;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->mediaHelper = new MediaHelper(
      $this->getConfigFactoryStub([
        'media_helper.settings' => [],
      ]),
      $this->createMock(EntityTypeManagerInterface::class),
      $this->createMock(FileSystemInterface::class),
      $this->createMock(ModuleHandlerInterface::class),
    );
  }

  /**
   * Tests type-getting method.
   */
  public function testMediaBundle(): void {
    $mock_medias = [];
    foreach (['image', 'video'] as $mock_media_bundle) {
      $mock_media = $this->createMock(MediaInterface::class);
      $mock_media->method('bundle')->willReturn($mock_media_bundle);
      $mock_medias[] = $mock_media;
    }

    $this->assertSame($mock_medias[0]->bundle(), $this->mediaHelper->mediaBundle($mock_medias[0]), 'mediaBundle() should return the media entity\'s bundle when passed a single entity.');

    $all_one_type = array_fill(0, 3, $mock_medias[0]);
    $this->assertSame($mock_medias[0]->bundle(), $this->mediaHelper->mediaBundle($all_one_type), 'mediaBundle() should return the media entity\'s bundle when passed an array of entities all of a single bundle.');

    $this->assertSame('mixed', $this->mediaHelper->mediaBundle($mock_medias), 'mediaBundle() should return the string "mixed" when passed an array of media entities of different bundles.');
  }

  /**
   * Tests source-type-getting method.
   */
  public function testMediaSource(): void {
    $mock_medias = [];
    foreach (['image', 'file', 'video_file'] as $mock_source_id) {
      $mock_source = $this->createMock(MediaSourceInterface::class);
      $mock_source->method('getPluginId')->willReturn($mock_source_id);

      $mock_media = $this->createMock(MediaInterface::class);
      $mock_media->method('getSource')->willReturn($mock_source);
      $mock_medias[] = $mock_media;
    }

    $this->assertSame($mock_medias[0]->getSource()->getPluginId(), $this->mediaHelper->mediaSource($mock_medias[0]), 'mediaSource() should return the media source id when passed a single entity.');

    $all_one_type = array_fill(0, 3, $mock_medias[0]);
    $this->assertSame($mock_medias[0]->getSource()->getPluginId(), $this->mediaHelper->mediaSource($all_one_type), 'mediaSource() should return the media source id when passed an array of entities all of a single source type.');

    $this->assertSame('mixed', $this->mediaHelper->mediaSource($mock_medias), 'mediaSource() should return the string "mixed" when passed an array of media entities of different source types.');
  }

}
