# Media Helper

This module serves two main purposes:

1. Making it easier to customize the output of image-based media entities via
display formatter. The image style, custom classes, and other HTML attributes
can be specified directly in the view display context for the media reference
field. (When using core display formatters, most of these things would require
PHP preprocessing and/or setting up a "pass through" Media view mode for unique
set of attributes used.)
1. Making it easier to output file-based media entities like image- and video-
based media entities in Twig. The image style, custom classes, and other HTML
attributes can be specified directly in Twig. Cache metadata for the media
entity and file upload are handled automatically just like one would expect from
a display formatter.

The `media_helper` service that powers the additional formatters and Twig
functions provides public methods that can assist with programmatic rendering.


## Requirements

This module depends on the Drupal core Image and Media modules.


## Integrations

This module optionally integrates with these other modules:


### Responsive Image
The Responsive Image module from Core is automatically supported if it is
enabled.


### SVG Image Field
If the [SVG Image Field module](https://www.drupal.org/project/svg_image_field)
is installed, optional integration must be enabled in Media Helper settings in
order for the SVG field and media types to be supported by Media Helper.

An enhancement to set/override SVG &lt;img&gt; dimension attributes based on
the image styles passed to Media Helper is also optional and enabled by default.


### SVG Image
If the [SVG Image module](https://www.drupal.org/project/svg_image) is
installed, optional integration is enabled by default that enhances the markup
of SVGs output via this module.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
1. Optionally configure integrations and other settings at
Administration > Configuration > Media > Media Helper.


## Twig

The Twig filters and functions provided by this module accept any of the
following as their input or first argument:
* A media ID, e.g. `6` or `'6'`.
* A media entity object.
* An entity reference field object. (e.g. `node.field_my_media_reference_field`)
* Field render arrays, e.g. `content.field_my_media_reference_field`. ("Display
Entity" format is supported, but other formats may not be.)

Although not all examples in this README will give examples of all four
signatures, all Media Helper Twig filters and functions do support all four.


### Functions

The module adds Twig functions to easily retrieve information about media
entities.


#### media_bundle

Returns the bundle ID for the media entity object.

Example usage:
```
{{ media_bundle(6) }}

{{ media_bundle(media_entity_object) }}

{{ media_bundle(content.field_media) }}

{% if media_bundle(node.field_media) == 'image' %}
  <p>All media entities in field_media are of type "image"</p>
{% endif %}
```

If there is no identifiable media entity, NULL will be returned.

If there are multiple entities all of the same bundle, that bundle ID will be
returned.

If there are multiple entities of different bundles, the string `'mixed'` will
be returned.


#### media_source

Returns the source type ID for the media entity object.

Example usage:
```
{{ media_source(6) }}

{{ media_source(media_entity_object) }}

{{ media_source(content.field_media) }}

{% if media_source(node.field_media) == 'image' %}
  <p>All media entities in field_media have the source type "image"</p>
{% endif %}
```

If there is no identifiable media entity, NULL will be returned.

If there are multiple entities all having the same media source, that source ID
will be returned.

If there are multiple entities of different sources, the string `'mixed'` will
be returned.


### Filters

The module adds Twig filters for rendering images and videos from media
entities.


#### media_image

Outputs a full render array (resulting in an &lt;img&gt; or &lt;picture&gt; tag)
for the passed media entity or entities.

Example usage:
```
{{ 6|media_image }}

{{ media_entity_object|media_image }}

{{ node.field_media_image|media_image('500x500') }}

{{ content.field_media_image|media_image }}
```

All arguments are optional.

The first argument if specified is the image style that should be used. If not
specified, the image style on the media type's default display mode will be used
instead - or if that cannot be discoverd, the original image. Often it is the
original image that will end up being the fallback, and so although the image
style argument is optional, it is highly recommended to use it.

The Responsive Image module is also supported transparently. Specify the machine
name of a responsive image style to use it. (Identical machine names in the two
modules will resolve to the basic, non-responsive style. Don't give a responsive
style the same machine name as a basic image style if you want to use the
responsive one in Media Helper.)

Class(es) to add to the &lt;img&gt; or &lt;picture&gt; tag may also be specified
as a second argument. Examples:
```
{{ node.field_media_image|media_image('500x500', 'hi there') }}

{{ node.field_media_image|media_image('large', ['hi', 'there']) }}
```

Any HTML attribute may be added via a hash passed as a third argument. Examples:
```
{{ node.field_media_image|media_image(
  '500x500',
  'my-class',
  { loading: 'eager' }
) }}

{{ node.field_media_image|media_image('large', '', { 'data-foo': 'bar' }) }}
```


#### media_image_url

Outputs just the image URL for the passed media entity. This is primarily
intended for use with an inline `background-image` CSS value.

Example usage:
```
{{ media_entity_object|media_image_url }}
{{ node.field_media_image|media_image_url('500x500') }}
```

Like `media_image`, `|media_image_url` accepts an image style as an argument.

Note that this filter still provides a render array that must be output, e.g.
using `{{ }}` in Twig. (Using a full render array instead of a string is
necessary for cache metadata.) Because of this, if you have enabled Twig
debugging, all of that debugging output will appear around the URL. This filter
is most likely to be used inside an HTML attribute such as `style`, where any
debug output will likely prevent the URL from working properly.

To ensure this filter works in dev environments that Twig debugging turned on,
you may need to string several more filters together to ensure the debug output
is stripped if it is present:
```
image|media_image_url|render|striptags|trim
```

Note that this solution will escape the `?` query parameter character in the
URL, which could pose a problem for any web servers that do not unescape the
character. If you encounter this issue with your web server, you may need to add
the `|raw` filter to work around it:
```
image|media_image_url|render|striptags|trim|raw
```


#### media_video

Outputs a render array for a &lt;video&gt; tag for the passed media entity or
entities.

This is useful for specifying custom Drupal display widget settings.

The display widget has different defaults than Drupal's `file_video` widget:
```
'autoplay'                   => TRUE,
'controls'                   => FALSE,
'loop'                       => TRUE,
'muted'                      => TRUE,
'multiple_file_display_type' => 'sources',
'width'                      => NULL,
'height'                     => NULL,
```

If autoplay is left as truthy, a few additional HTML attributes will be added to
the &lt;video&gt; tag:
```
'disablePictureInPicture' => 'true',
'playsinline'             => TRUE,
```

The intent of these defaults is to make it straightforward to use videos in a
more decorative manner that is similar to images. Drupal's defaults tend to
assume a front-and-center video with controls.

This filter is also intended to enable setting HTML attributes directly in a
larger template, without having to configure view modes or set up separate twig
templates for the video file or media entity specifically.

Arguments are available and are all optional:
* The first argument is one or more CSS class names (string or array).
* The second argument takes a hash and allows you to pass settings for the
`file_video` field widget, either adding to or overriding the defaults shown
above.
* The third argument takes a hash and allows you to pass custom attributes for
the &lt;video&gt; tag.

Example usage:
```
{{ node.field_media_video|media_video('my-custom-class') }}

{{ node.field_media_video|media_video('', { width: 200, height: 300 }) }}

{{ node.field_media_video|media_video(
  ['a-custom-class', 'or-two'],
  {},
  { 'data-foo': 'bar' }
)}}
```


#### media_first_nonempty

Takes an array (or other iterable) and returns the first value that is a valid
media input.

Example usage:
```
{{ [
  node.field_optional_image,
  node.field_another_optional_image,
  guaranteed_value
]|media_first_nonempty|media_image('500x500') }}
```

This is effectively equivalent to
```
{{
  node.field_optional_image|media_image('500x500')
  ?: node.field_another_optional_image|media_image('500x500')
  ?: guaranteed_value|media_image('500x500')
}}
```
but avoids duplication of arguments passed to later filters.


## Maintainers

- Ben Voynick - [bvoynick](https://www.drupal.org/u/bvoynick)
- Nate Mow - [natemow](https://www.drupal.org/u/natemow)
- Heitor Althmann - [halth](https://www.drupal.org/u/halth)
