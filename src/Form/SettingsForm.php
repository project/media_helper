<?php

namespace Drupal\media_helper\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\media_helper\MediaHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Media Helper.
 */
class SettingsForm extends ConfigFormBase {

  public const HTML_TAGS_FOR_T = [
    '@img_tag'     => '<img>',
    '@picture_tag' => '<picture>',
    '@source_tag'  => '<source>',
  ];

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructs the Media Helper settings form.
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typed_config_manager, ModuleHandlerInterface $module_handler) {
    parent::__construct($config_factory, $typed_config_manager);

    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_helper_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'media_helper.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->configFactory->get('media_helper.settings');

    $form['integrations'] = [
      '#type' => 'container',
      '#tree' => TRUE,
    ];

    $form['integrations']['responsive_image'] = [
      '#title' => $this->t('Responsive Image module integration'),
      '#type' => 'fieldset',
      '#tree' => TRUE,

      'img_tag_attributes' => [
        '#type'  => 'textarea',
        '#title' => $this->t('@img_tag tag attributes', static::HTML_TAGS_FOR_T),
        '#description' => $this->t(
          'Customize what attributes passed to mediaImage() go on the @img_tag rather than @picture_tag when rendering responsive images. Enter one attribute per line.<br>The following attributes will always go on @img_tag no matter what, and do not need to be listed here:<br>@hardcoded_tags',
          self::HTML_TAGS_FOR_T + [
            '@hardcoded_tags' => Markup::create('<ul><li>' . implode('</li><li>', MediaHelperInterface::ALWAYS_IMG_TAG_ATTRS) . '</li></ul>'),
          ]
        ),
        '#default_value' => implode(
          "\n",
          $config->getOriginal('integrations.responsive_image.img_tag_attributes', FALSE) ?: []
        ),
        '#attributes' => [
          'spellcheck' => 'false',
        ],
      ],

      'module_installed' => $this->moduleInstalledMarkup('responsive_image', 'Responsive Image'),
    ];

    $svg_image_module_enabled = $this->moduleHandler->moduleExists('svg_image');
    $svg_image_integration_enabled = $config->getOriginal('integrations.svg_image.enable', FALSE);
    $form['integrations']['svg_image'] = [
      '#title' => $this->t('SVG Image module integration'),
      '#type' => 'fieldset',
      '#tree' => TRUE,

      'enable' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable integration'),
        '#description' => $this->t(
          'Enables integration with the <a href="@svg_image_url" target="_blank">SVG Image</a> module.<br>When an SVG image is output via Media Helper, <code>&lt;img&gt;</code> tag width and height attributes will be set based on the image style specified.<br>This integration is not required in order to use SVG Image together with Media Helper, but it is recommended.',
          ['@svg_image_url' => 'https://www.drupal.org/project/svg_image'],
        ),
        '#default_value' => $svg_image_integration_enabled,
        '#disabled' => !$svg_image_module_enabled && !$svg_image_integration_enabled,
      ],

      'module_installed' => $this->moduleInstalledMarkup('svg_image', 'SVG Image'),
    ];

    $svg_image_field_module_enabled = $this->moduleHandler->moduleExists('svg_image_field');
    $svg_image_field_integration_enabled = $config->getOriginal('integrations.svg_image_field.enable', FALSE);
    $svg_image_field_dimension_override_enabled = $config->getOriginal('integrations.svg_image_field.override_dimensions', FALSE);
    $form['integrations']['svg_image_field'] = [
      '#title' => $this->t('SVG Image Field module integration'),
      '#type' => 'fieldset',
      '#tree' => TRUE,

      'enable' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable integration'),
        '#description' => $this->t(
          'Enables integration with the <a href="@svg_image_field_url" target="_blank">SVG Image Field</a> module.<br>If this integration is not enabled, media types based on SVG Image Field cannot be output using Media Helper.',
          ['@svg_image_field_url' => 'https://www.drupal.org/project/svg_image_field'],
        ),
        '#default_value' => $svg_image_field_integration_enabled,
        '#disabled' => !$svg_image_field_module_enabled && !$svg_image_field_integration_enabled,
      ],

      'override_dimensions' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Override image dimensions'),
        '#description' => $this->t('If enabled, the dimension attributes of SVG images output via Media Helper will be overridden to conform to the specified image style, similar to the SVG Image module integration.<br>If disabled, the image style specified will be disregarded for SVG Image Field images. Either the dimensions of the original SVG file will be used, or no dimension attributes will be output on the <code>&lt;img&gt;</code> tag at all.'),
        '#default_value' => $svg_image_field_dimension_override_enabled,
        '#disabled' => !$svg_image_field_module_enabled && !$svg_image_field_dimension_override_enabled,
      ],

      'module_installed' => $this->moduleInstalledMarkup('svg_image_field', 'SVG Image Field'),
    ];

    $build = parent::buildForm($form, $form_state);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $allowed_img_tags = $form_state->getValue(['integrations', 'responsive_image', 'img_tag_attributes']) ?? '';
    $allowed_img_tags = preg_split('/\s*\R\s*/', $allowed_img_tags, 0, PREG_SPLIT_NO_EMPTY);
    $allowed_img_tags = array_unique($allowed_img_tags);
    // Remove any tags that are hardcoded to go on the image.
    foreach ($allowed_img_tags as $tag_key => &$allowed_tag) {
      $allowed_tag = strtolower($allowed_tag);
      if (in_array($allowed_tag, MediaHelperInterface::ALWAYS_IMG_TAG_ATTRS, TRUE)) {
        unset($allowed_img_tags[$tag_key]);
      }
    }
    $allowed_img_tags = array_values($allowed_img_tags);
    sort($allowed_img_tags);
    $form_state->setValue(['integrations', 'responsive_image', 'img_tag_attributes'], $allowed_img_tags);

    foreach ($allowed_img_tags as $allowed_img_tag) {
      if (!preg_match('/^[^\t\n\f \/<>"\'=]+$/', $allowed_img_tag)) {
        $form_state->setErrorByName('integrations][responsive_image][img_tag_attributes', '"' . $allowed_img_tag . '" is not a valid HTML attribute.');
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $data = [
      'integrations' => $form_state->getValue('integrations'),
    ];

    $this->config('media_helper.settings')
      ->setData($data)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Generates boilerplate form element markup informing the user if a module is enabled.
   */
  protected function moduleInstalledMarkup(string $module_id, string $fallback_name): array {
    $module_is_installed = $this->moduleHandler->moduleExists($module_id);
    $module_name = $module_is_installed
      ? $this->moduleHandler->getName($module_id)
      : $fallback_name;
    return [
      '#markup' => $this->t(
        // phpcs:ignore Drupal.Semantics.FunctionT.NotLiteralString
        $module_is_installed
          ? '<p>The @module_name module is installed.</p>'
          : '<p>The @module_name module is <strong>not</strong> installed. This integration has no effect.</p>',
        ['@module_name' => $module_name],
      ),
    ];
  }

}
