<?php

namespace Drupal\media_helper\EventSubscriber;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Reacts to changes in this module's config.
 */
class ConfigSubscriber implements EventSubscriberInterface {

  /**
   * Constructs the subscriber.
   */
  public function __construct(
    protected CacheTagsInvalidatorInterface $cacheTagsInvalidator,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE   => 'configAltered',
      ConfigEvents::DELETE => 'configAltered',
    ];
  }

  /**
   * Invalidate render cache on save.
   */
  public function configAltered(ConfigCrudEvent $event): void {
    if ($event->getConfig()->getName() === 'media_helper.settings') {
      $this->cacheTagsInvalidator->invalidateTags([
        'rendered',
      ]);
    }
  }

}
