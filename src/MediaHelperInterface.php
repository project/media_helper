<?php

namespace Drupal\media_helper;

use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceInterface;

/**
 * Interface for the main media helper service.
 */
interface MediaHelperInterface {

  /**
   * A list of HTML attributes that should always go on the <img> element.
   *
   * @var string[]
   */
  public const ALWAYS_IMG_TAG_ATTRS = [
    'alt',
    'crossorigin',
    'decoding',
    'height',
    'loading',
    'referrerpolicy',
    'src',
    'srcset',
    'sizes',
    'width',
  ];

  /**
   * Get the IDs of image source plugins that the media helper module can handle.
   */
  public function getSupportedImageSourcePluginIds(): array;

  /**
   * Check if a given media source plugin can be handled by media helper image processing.
   */
  public function isImageSourceSupported(string|MediaSourceInterface $source): bool;

  /**
   * Check if a given module integration is enabled.
   *
   * This method is preferable to direct checks using the moduleHandler, because enabled modules might still not have
   * their integration enabled for a few reasons:
   *   - Most integrations can be disabled in configuration.
   *   - An integration might not pass sanity/safety checks specific to the integrated module.
   */
  public function isModuleIntegrationEnabled(string $module): bool;

  /**
   * Get the names of HTML attributes that should be on the <img> tag.
   *
   * @return string[]
   *   The HTML attribute names that should be on the <img> tag instead of <picture>.
   */
  public function getResponsiveImgTagAttributeNames(): array;

  /**
   * Get the type for the given media entity(s).
   *
   * @param \Drupal\media\MediaInterface|\Drupal\media\MediaInterface[] $media
   *   A media entity object or an array of them.
   *
   * @return string|null
   *   The bundle ID of the passed media.
   *   If an empty array is passed, NULL will be returned.
   *   If there are multiple entities all of the same bundle, that bundle ID will be returned.
   *   If there are multiple entities of different bundles, the string "mixed" will be returned.
   */
  public function mediaBundle(MediaInterface|array $media): ?string;

  /**
   * Get the source ID for the given media entity(s).
   *
   * @param \Drupal\media\MediaInterface|\Drupal\media\MediaInterface[] $media
   *   A media entity object or an array of them.
   *
   * @return string|null
   *   The ID of the source of the passed media.
   *   If an empty array is passed, NULL will be returned.
   *   If there are multiple entities all having the same media source, that source ID will be returned.
   *   If there are multiple entities of different sources, the string "mixed" will be returned.
   */
  public function mediaSource(MediaInterface|array $media): ?string;

  /**
   * Render a media image.
   *
   * @param \Drupal\media\MediaInterface|\Drupal\media\MediaInterface[]|null $media
   *   A media entity object or an array of them.
   * @param string $style
   *   (optional) The machine name of the image style to use. If not specified, the image style used on the media's
   *   default display mode will be used instead - or if that cannot be discovered, the original image.
   *   Also supports Responsive Image module transparently. Specify the machine name of a responsive image style to use
   *   it. (Identical machine names in the two modules will resolve to the basic, non-responsive style.)
   * @param string[]|string $classes
   *   (optional) Class(es) to add to the element, as a string or array of strings.
   * @param array $attributes
   *   (optional) Attributes to add to the element, name => value.
   *
   * @return array|null
   *   A render array, or NULL if there is no meaningful input or output.
   */
  public function mediaImage(MediaInterface|array|null $media, string $style = '', array|string $classes = [], array $attributes = []): ?array;

  /**
   * Render a media image URL.
   *
   * @param \Drupal\media\MediaInterface|\Drupal\media\MediaInterface[]|null $media
   *   A media entity object or an array of them.
   * @param string $style
   *   (optional) The machine name of the image style to use.
   *
   * @return array|null
   *   A render array, or NULL if there is no meaningful input or output.
   *   (A render array must be used for cache metadata bubbling.)
   */
  public function mediaImageUrl(MediaInterface|array|null $media, string $style = ''): ?array;

  /**
   * Render a media uploaded video.
   *
   * @param \Drupal\media\MediaInterface|\Drupal\media\MediaInterface[] $media
   *   A media entity object or an array of them.
   * @param string[]|string $classes
   *   (optional) Class(es) to add to the <video> element, as a string or array
   *   of strings.
   * @param array $settings
   *   (optional) Settings overrides for the Drupal file_video display widget.
   *   These defaults are used:
   *     'autoplay' => TRUE
   *     'controls' => FALSE
   *     'loop' => TRUE
   *     'muted' => TRUE
   *     'multiple_file_display_type' => 'sources'
   *     'width' => NULL
   *     'height' => NULL
   *   and can be overridden by setting the value in this argument.
   * @param array $attributes
   *   (optional) Attributes to add to the <video> element, name => value.
   *   If the autoplay setting is on (which it is by default), the attributes 'disablePictureInPicture' and
   *   'playsinline' are added if not already set.
   *
   * @return array|null
   *   A render array, or NULL if there is no meaningful input or output.
   */
  public function mediaVideo(MediaInterface|array $media, array|string $classes = [], array $settings = [], array $attributes = []): ?array;

}
